################################################################
# Author: Mohit Mangal
# Email: mohit.mangal2@gmail.com
# File Name: sync.py
# Description: Upload data to google drive
################################################################
import os, sys
from shutil import copyfile
import pickle
import logging
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload
import configparser
from datetime import datetime
import time
from __main__ import lock
import asyncio

# Global directories
cwd = os.path.dirname(os.path.realpath(__file__))
config_folder = os.path.join(cwd, "..", "config")
data_folder = os.path.join(cwd, "..", "data")

logger = logging.getLogger("sync")
logger.setLevel(logging.ERROR)

# Reading configuration
try:
    config = configparser.ConfigParser()
    config.read(os.path.join(config_folder, "ip_camera.config"))
    file_name = config["DEFAULT"]["file_name"].strip()
    drive_folder = config["DEFAULT"]["drive_folder"].strip()
    upload_delay_sec = int(config["DEFAULT"]["upload_delay_sec"].strip())
    delete_batch = int(config["DEFAULT"]["delete_batch"].strip())
except Exception as e:
    logger.error("Failed to read configuration {}".format(e))

upload_start = False
creds = None
service = None
folderId = None

def getCredentials():
    logger.debug("getCredentials: enter")
    try:
        SCOPES = ['https://www.googleapis.com/auth/drive']
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(os.path.join(config_folder, 'token.pickle')):
            with open(os.path.join(config_folder, 'token.pickle'), 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    os.path.join(config_folder, 'credentials.json'), SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(os.path.join(config_folder, 'token.pickle'), 'wb') as token:
                pickle.dump(creds, token)
        logger.debug("getCredentials: exit")
        return creds
    except Exception as e:
        logger.error("getCredentials error: {}".format(e))

def upload(upload_file):
    """
    Upload file to google drive
    """
    logger.debug("upload: enter")
    if not creds:
        logger.error("upload error: failed to get creds")
        return
    
    file_metadata = {'name': upload_file, 'parents': [folderId]}
    media = MediaFileUpload(os.path.join(data_folder, upload_file), mimetype='image/png')
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    logger.debug("upload: exit")

def call_back(request_id, response, exception):
    logger.debug("call_back: enter")
    if exception is not None:
        logger.error("call_back error: {}".format(exception))
    else:
        logger.debug("call_back: exit")

def create_space_in_drive():
    """
    This function creates space in drive
    Paremeters:
        folderId: ID of folder which needs to be deleted
    """
    logger.debug("create_space_in_drive: enter")
    try:
        batch = service.new_batch_http_request(callback=call_back)
        file_count = 0
        page_token = None
        while file_count<delete_batch:
            param = {"orderBy":"createdTime"}
            if page_token:
                param['pageToken'] = page_token
            children = service.files().list(**param).execute()
            for child in children.get('files', []):
                if not child['name'].endswith('.png'):
                    continue
                batch.add(service.files().delete(fileId=child['id']))
                file_count += 1
                if file_count>=delete_batch:
                    break
            page_token = children.get('nextPageToken')
            if not page_token:
                break
        batch.execute()
    except Exception as e:
        logger.error("create_space_in_drive error: {}".format(e))
    logger.debug("create_space_in_drive: exit")

def sync():
    """
    This function sync data
    """
    logger.debug("sync: enter")
    try:
        file = os.path.join(data_folder,file_name)
        global upload_start, creds, service, folderId
        upload_start = True
        while True:
            try:
                creds = getCredentials()
                break
            except Exception as e:
                logger.error("sync error: failed to get credentials {}".format(e))

        service = build('drive', 'v3', credentials=creds)
        results = service.files().list(q="name='"+drive_folder+"'", pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        if not items:
            logger.error("sync error: failed to find any folder")
            return
        else:
            for item in items:
                if item['name']==drive_folder:
                    folderId = item['id']
                    break
        if not folderId:
            logger.error("sync error: failed to find maingate folder")
            return
        while upload_start:
            try:
                newFile = datetime.now().strftime("%d-%m-%Y %H-%M-%S")+".png"
                with lock:
                    os.rename(os.path.join(data_folder, file_name), os.path.join(data_folder, newFile))

                upload(os.path.join(data_folder, newFile))
                time.sleep(upload_delay_sec)
            except Exception as e:
                logger.error("sync error in loop: {}".format(e))
                if "Drive storage quota has been exceeded" in str(e):
                    create_space_in_drive()
            # Deleting old files
            for upload_file in os.listdir(data_folder):
                if not upload_file==file_name:
                    os.remove(os.path.join(data_folder, upload_file))
    except Exception as e:
        logger.error("sync error: {}".format(e))
    logger.debug("sync: exit")

def stop_sync():
    """
    This function stops synronization
    """
    logger.debug("stop_sync: enter")
    global upload_start
    upload_start = False
    logger.debug("stop_sync: exit")

if __name__=="__main__":
    start_sync()
    stop_sync()
