################################################################
# Author: Mohit Mangal
# Email: mohit.mangal2@gmail.com
# File Name: view.py
# Description: IP camers file to read and detect persons
################################################################
import os, sys
import requests
import time
import numpy as np
import cv2
import logging
import configparser
import queue
from __main__ import lock

# Global directories
cwd = os.path.dirname(os.path.realpath(__file__))
config_folder = os.path.join(cwd, "..", "config")
logs_folder = os.path.join(cwd, "..", "logs")
data_folder = os.path.join(cwd, "..", "data")

logger = logging.getLogger("view")

# Reading configuration
try:
    config = configparser.ConfigParser()
    config.read(os.path.join(config_folder, "ip_camera.config"))
    ip = config["DEFAULT"]["ip"].strip()
    user = config["DEFAULT"]["user"].strip()
    password = config["DEFAULT"]["password"].strip()
    file_name = config["DEFAULT"]["file_name"].strip()
    stream_url = config["DEFAULT"]["stream_url"].strip()
    position_url = config["DEFAULT"]["position_url"].strip()
    start_camera_sec = int(config["DEFAULT"]["start_camera_sec"].strip())
except Exception as e:
    logger.error("Failed to read configuration {}".format(e))

class View:
    """
    This class captures image from ip camera
    """
    def __init__(self):
        """
        __init__ function: initializes stream variable
        """
        logger.debug("__init__: enter")
        self._ip_camera_frame = None
        logger.debug("__init__: exit")

    def __del__(self):
        """
        __del__ function: releases capture from ip camers is in progress
        """
        logger.debug("__del__: enter")
        if self._ip_camera_frame:
            self._ip_camera_frame.release()
        logger.debug("__del__: exit")

    def _set_position(self):
        """
        This function sets camera position
        """
        logger.debug("_set_position: enter")
        urlPosition = position_url.format(ip, user, password)
        while True:
            try:
                requests.get(urlPosition)
                time.sleep(20)                   # sleep to give time to position camera
                break
            except Exception as e:
                logger.error("start _set_position: failed to set position {}".format(e))
        logger.debug("_set_position: exit")

    def _get_stream(self):
        """
        This function starts camera stream
        """
        logger.debug("_get_stream: enter")
        url = stream_url.format(ip, user, password)
        # Open a Capture video from webcam
        self._ip_camera_frame = cv2.VideoCapture(url)
        while True:
            try:
                self._ip_camera_frame = cv2.VideoCapture(url)
                if not self._ip_camera_frame.isOpened():
                    logger.error("_get_stream: cannot open videostream")
                else:
                    break
            except Exception as e:
                logger.error("_get_stream error: failed to get frame {}".format(e))
        logger.debug("_get_stream: exit")

    def start(self):
        """
        This function is the starts capture from ip camera
        """
        logger.debug("start: enter")
        try:
            self._get_stream()     # Start stream
            time.sleep(start_camera_sec)        # Sleep to give time to start camera
            self._set_position()   # Setting camera position

            while(True):
                # Capture frame-by-frame
                ret, frame = self._ip_camera_frame.read()
                if frame is not None:
                    # Display the resulting frame
                    imagePath = os.path.join(data_folder, file_name)
                    with lock:
                        cv2.imwrite(imagePath, frame)

                    if cv2.waitKey(22) & 0xFF == ord('q'):
                        break
                else:
                    logger.error("start: frame is none")
                    self._get_stream()     # Start stream 
                    time.sleep(start_camera_sec)        # Sleep to give time to start camera
                    self._set_position()   # Setting camera position
        except Exception as e:
            logger.error("start error: {}".format(e))
        logger.debug("start: exit")

if __name__=="__main__":
    start()
