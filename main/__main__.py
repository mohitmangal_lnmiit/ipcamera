################################################################
# Author: Mohit Mangal
# Email: mohit.mangal2@gmail.com
# File Name: __main__.py
# Description: IP camers file to get and upload image to google
# drive
################################################################
import threading
lock = threading.Lock()
import os, sys
import requests
import logging
from view import View
import sync

# Global directories
cwd = os.path.dirname(os.path.realpath(__file__))
logs_folder = os.path.join(cwd, "..", "logs")

# Logs Initialization
logs_file = os.path.join(logs_folder, "ipCamera.log")
os.makedirs(logs_folder, exist_ok=True)
if os.path.exists(logs_file):
    os.remove(logs_file)
logging.basicConfig(filename=logs_file, level=logging.ERROR,
                    format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger("__main__")

def main():
    """
    main function
    """
    logger.debug("main: enter")
    while True:
        try:
            requests.get("https://google.com")
            break
        except Exception as e:
            logger.error("main: internit available {}".format(e))
    startImg = threading.Thread(target=View().start)
    startSync = threading.Thread(target=sync.sync)

    logger.debug("main: starting taking images thread")
    startImg.start()

    logger.debug("main: starting synch thread")
    startSync.start()

    logger.debug("main: joining taking images thread")
    startImg.join()

    logger.debug("main: joining sync thread")
    startSync.join()

    sync.stop_sync()
    logger.debug("main: exit")

if __name__=="__main__":
    main()
